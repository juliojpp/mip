<%-- 
    Document   : index
    Created on : 02-09-2019, 18:37:25
    Author     : 404
--%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="javax.ws.rs.client.ClientBuilder"%>
<%@page import="javax.ws.rs.client.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
    String miip=request.getHeader("X-Forwarded-For");
    String t=miip;
    if (miip==null) {
        miip = request.getRemoteAddr();
    }
    String url = "https://api.ip2country.info/ip?"+ miip;
    
    Client client= ClientBuilder.newClient();
    Response r=client.target(url).request().get();
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Mi IP:</h3>
        <br>
        <%= r.readEntity(String.class)%> <br>
    </body>
</html>
